# pylisk

Python lisk API library package 
```
git clone https://github.com/slasheks/pylisk.git
cd pylisk
```
```
$ python3 
Python 3.6.5 (default, Apr  1 2018, 05:46:30) 
[GCC 7.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import pylisk
>>> lisk_api = pylisk.helpers.lskAPI("https://testnet.lisk.io")
>>> lisk_api.node('status')
```
```python
{'meta': {}, 'data': {'broadhash': '417cb888920f3cfe502cbe2ce1923e8069946b771bf45577f65e38a183f6dd48', 'consensus': 100, 'height': 6121893, 'loaded': True, 'networkHeight': 6121893, 'syncing': False, 'transactions': {'confirmed': 774109, 'unconfirmed': 0, 'unprocessed': 0, 'unsigned': 0, 'total': 774109}}, 'links': {}}
```
---

As an execution module

Examples:

    v1.0.0
    python3 -m pylisk get status
    python3 -m pylisk get constants

    python3 -m pylisk get peers
    python3 -m pylisk get peers --limit 1
    python3 -m pylisk get peers --offset 2
    python3 -m pylisk get peers --offset 2 --limit 2

    python3 -m pylisk get delegates
    python3 -m pylisk get delegates --limit 2
    python3 -m pylisk get delegates --offset 2
    python3 -m pylisk get delegates --offset 2 --limit 2

    python3 -m pylisk get forging --delegate slasheks

    python3 -m pylisk get forgers

    python3 -m pylisk get delegate --delegate slasheks
    python3 -m pylisk get delegate_search --delegate slash

    python3 -m pylisk get forging_statistics --delegate genesis_80

    python3 -m pylisk get transactions
    python3 -m pylisk get transactions --limit 1
    python3 -m pylisk get transactions --offset 1
    python3 -m pylisk get transactions --offset 1 --limit 1
    python3 -m pylisk get transaction --transaction 9046966103496425914

    python3 -m pylisk get blocks
    python3 -m pylisk get blocks --limit 1
    python3 -m pylisk get blocks --offset 1
    python3 -m pylisk get blocks --offset 1 --limit 1
    python3 -m pylisk get block --block 9046966103496425914

    python3 -m pylisk get votes --delegate slasheks
    python3 -m pylisk get voters --delegate slasheks
 
usage: 

    __main__.py [-h] [-e EXECUTION_MODE] [-i INSTALL_DIR]
                   [-li LISKY_CONFIG_PATH]
                   [--delegates DELEGATES [DELEGATES ...]]
                   [--blocks BLOCKS [BLOCKS ...]]
                   [--addresses ADDRESSES [ADDRESSES ...]]
                   [--transactions TRANSACTIONS [TRANSACTIONS ...]] [-u URL]
                   [-o OFFSET] [-l LIMIT] [-n {mainnet,testnet}] [-d DELEGATE]
                   [-b BLOCK] [-t TRANSACTION] [-a ADDRESS] [-p PUBLIC_KEY]
                   [--yaml]
                   {get,list,set}
                   {address,account,delegate,delegates,peers,votes,voters,status,sync,transaction,transactions,addresses,block,blocks,network,upvotes,downvotes,forging,fork,forks,constants,forgers,delegate_search,forging_statistics}

