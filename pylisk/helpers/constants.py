"""
"""
import random

class init:
    """
    Constant variables
    """
    def __init__(self):

        self.mainnet_url = "https://node01.lisk.io"
        self.testnet_url = "https://testnet.lisk.io"
        self.mainnet_address = "17653577803495162002L"
        self.testnet_address = "1L"
        self.mainnet_transaction = "4273442346685525340"
        self.testnet_transaction = "1234"
        self.offset = random.randint(1,5)
        self.limit = random.randint(1,5)
