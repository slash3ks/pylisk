"""
Lisk API Class
"""

#TODO paginate

import pylisk.lskutils

class lskAPI:
    """

    """

    def __init__(self, url):
        """

        """
        self.url = url

        forall_ = '/api/'

        self._loader = forall_ + 'loader'
        self._transactions = forall_ + 'transactions'
        self._delegates = forall_ + 'delegates'
        self._peers = forall_ + 'peers'
        self._node = forall_ + 'node'
        self._vote = forall_ + 'vote'
        self._blocks = forall_ + 'blocks'

    @staticmethod
    def _limoff(endpoint, offset, limit):
        """
        """
        if endpoint:
            sep = '&'
        else:
            sep = '?'

        if offset and limit:
            uri = '{}offset={}&limit={}'.format(sep, offset, limit)
        elif limit and not offset:
            uri = '{}limit={}'.format(sep, limit)
        elif offset and not limit:
            uri = '{}offset={}'.format(sep, offset)
        else:
            uri = ''

        return uri

    def delegates(self, rtype, offset, limit, delegate_name=None, delegate_address=None):
        """

        """

        if rtype == 'delegates':

            uri = self._limoff(None, offset, limit)

        elif rtype == 'forgers':

            uri = '/forgers' + self._limoff(None, offset, limit, None)

        elif rtype == 'delegate':

            if not delegate_name:
                return {'success':False,
                        'message':'Missing --delegate <delegate name>'}

            uri = '?username={}'.format(delegate_name)

        elif rtype == 'delegate_search':

            if not delegate_name:
                return {'success':False,
                        'message':'Missing --delegate <delegate name>'}

            uri = '?search={}'.format(delegate_name) + self._limoff(True, offset, limit)

        elif rtype == 'forging_statistics':

            if delegate_name:

                delegate_data = self.delegates('delegate', None, None, delegate_name)
                try:
                    address = delegate_data['data'][0]['account']['address']
                except IndexError:
                    return {'success': False,
                            'message': 'Delegate not found'}

            elif delegate_address:

                address = delegate_address

            else:

                return {'success': False,
                        'message': 'Option {} not supported'.format(rtype)}

            uri = '/{}/forging_statistics'.format(address)

        else:

            return {'success': False,
                    'message': 'Option {} not supported'.format(rtype)}

        return pylisk.lskutils.http_get(self.url, self._delegates + uri, None)

    def votes(self, rtype, offset, limit, delegate_name=None, delegate_address=None):
        """

        """

        if rtype == 'votes':

            if not delegate_name:
                return {'success':False,
                        'message':'Missing --delegate <delegate name>'}

            uri = 's?username={}'.format(delegate_name)

        elif rtype == 'voters':

            if not delegate_name:
                return {'success':False,
                        'message':'Missing --delegate <delegate name>'}

            uri = 'rs?username={}'.format(delegate_name)

        else:

            return {'success': False,
                    'message': 'Option {} not supported'.format(rtype)}

        uri = uri + self._limoff(True, offset, limit)

        return pylisk.lskutils.http_get(self.url, self._vote + uri, None)

    def peers(self, rtype, offset, limit):
        """
        v1.0
        """
        if rtype == 'peers':

            uri = self._limoff(None, offset, limit)

        else:
            return {'success': False,
                    'message': 'Option {} not supported'.format(rtype)}

        return pylisk.lskutils.http_get(self.url, self._peers + uri, None)

    def transactions(self, rtype, offset, limit, transaction=None):
        """

        """

        if rtype == 'transaction':

            if not transaction:
                return {'success':False,
                        'message':'Missing --transaction <tx-id>'}

            uri = '?id={}'.format(transaction) 

        elif rtype == 'transactions':

            uri = '' + self._limoff(None, offset, limit)

        return pylisk.lskutils.http_get(self.url, self._transactions + uri, None)

    def blocks(self, rtype, offset, limit, block=None):
        """

        """

        if rtype == 'block':

            if not block:
                return {'success':False,
                        'message':'Missing --block <block-id>'}

            uri = '?blockId={}'.format(block)

        elif rtype == 'blocks':

            uri = '' + self._limoff(None, offset, limit)

        else:

            return None

        return pylisk.lskutils.http_get(self.url, self._blocks + uri, None)

    def node(self, rtype, delegate_name=None):
        """
        v1.0.0
        """
        if rtype == 'constants':
            uri = '/constants'
        elif rtype == 'status':
            uri = '/status'
        elif rtype == 'forging':

            delegate_data = self.delegates('delegate', None, None, delegate_name)
            try:
                pubkey = delegate_data['data'][0]['account']['publicKey']
            except IndexError:
                return {'success': False,
                        'message': 'Delegate not found'}

            uri = '/status/forging?publicKey={}'.format(pubkey)
        else:
            return {'success': False,
                    'message': 'Option {} not supported'.format(rtype)}

        return pylisk.lskutils.http_get(self.url, self._node + uri, None)
