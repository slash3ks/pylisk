"""
Lisky Class
"""

#TODO debug that command

import json
import pylisk.lskutils

class LSKy:
    """

    """

    def __init__(self, install_dir):
        """

        """
        self.lisk_node = install_dir + 'bin/node'
        self.lisky_app = install_dir + 'bin/lisky'


    def get(self, option, arguments):
        """

        """
        file_check = pylisk.lskutils.check_files([self.lisk_node, self.lisky_app])
        if file_check: return file_check

        available_options = ['account', 'address', 'block',
                             'delegate', 'transaction']

        if option not in available_options:

            msg_str = 'Selected option is not valid. Try: {}'\
                       .format(available_options)

            return {'success': False,
                    'message': 'Selected option is not valid.'}

        cmd = 'get {} {}'.format(option, arguments)
        command = '{} {} {} --json'.format(self.lisk_node, self.lisky_app, cmd)
        response = pylisk.lskutils.run_cmd(command.split())

        return response

    def list(self, option, arguments):
        """

        """
        file_check = pylisk.lskutils.check_files([self.lisk_node, self.lisky_app])
        if file_check: return file_check

        available_options = ['delegates', 'blocks', 'addresses']

        if option not in available_options:

            msg_str = 'Selected option is not valid. Try: {}'\
                       .format(available_options)

            return {'success': False,
                    'message': 'LISKYHELPER: Selected option is not valid.'}

        cmd = 'list {} {}'.format(option, ' '.join(arguments))
        command = '{} {} {} --json'.format(self.lisk_node, self.lisky_app, cmd)
        response = pylisk.lskutils.run_cmd(command.split())

        return response

    def lisky_config_opt(self, lisky_config_path, key, value):
        """

        """

        with open(lisky_config_path, 'r+') as liskycfg:

            config = json.load(liskycfg)

            try:
                config['liskJS'][key] = value
            except KeyError:
                reponse = {'success': False,
                           'message': 'key {} does not exist in the config'\
                                      .format(key)}
            liskycfg.seek(0)

            try:
                json.dump(config, liskycfg)
                liskycfg.truncate()
            except:
                raise

            return {'success': True,
                    'message': 'Successfully wrote {}:{}'.format(key, value)}

    def set(self, rtype, lisky_config_path, target_network):
        """

        """

        file_check = pylisk.lskutils.check_files([lisky_config_path])
        if file_check: return file_check

        available_options = ['network', 'port']


        if rtype == 'network':

            if target_network == 'testnet':

                response = self.lisky_config_opt(lisky_config_path,
                                                 'testnet', True)

            elif target_network == 'mainnet':

                response = self.lisky_config_opt(lisky_config_path,
                                                 'testnet', False)
        else:

            response = {'success': False, 'message': 'dont do that'}

        return response
