"""
lskDB Class
"""

import pylisk.lskutils

class lskDB:
    """

    """

    def __init__(self, config_file):
        """

        """
        self.config_file = config_file
        self.del_addr_sql = '''
            SELECT m."username", m."isDelegate", m."u_isDelegate",
            m."secondSignature", m."u_secondSignature",
            m."u_username", m."address", m."balance",
            m."u_balance", m."vote", m."rate",
            m."delegates", m."u_delegates", m."address",
            m."multisignatures", m."u_multisignatures",
            m."multimin", m."u_multimin", m."multilifetime",
            m."u_multilifetime", m."blockId", m."nameexist",
            m."u_nameexist", m."producedblocks", m."missedblocks",
            m."fees", m."rewards", m."virgin",
            ENCODE(m."publicKey", \'hex\') AS public_key,
            ENCODE(m."secondPublicKey", \'hex\') AS second_public_key
            FROM mem_accounts as m'''
        self.transaction_sql = '''
            SELECT t."t_id", t."b_height", t."t_blockId",
            t."t_type", t."t_timestamp", t."t_senderId",
            t."t_recipientId", t."t_amount", t."t_fee",
            t."t_signature", t."t_SignSignature", t."t_signatures",
            t."confirmations",
            ENCODE(t."t_senderPublicKey", \'hex\') AS public_key
            FROM trs_list AS t'''

    def peers(self, rtype):
        """

        """

        if rtype == 'peers':

            sql = 'SELECT ip, port, state, os, version, height, clock FROM peers'

        return pylisk.lskutils.db_select(sql, None, self.config_file)

    def transactions(self, rtype, transaction_id=None, transactions=None):
        """

        """

        sql = self.transaction_sql
        sql_args = None

        if rtype == 'transaction':

            if not transaction_id: return {'success': False,
                                     'message': 'Missing --transaction <transaction>'}

            sql = sql + ' WHERE t_id = %(transaction_id)s'
            sql_args = {'transaction_id': transaction_id}

        elif rtype == 'transactions':

            if not transactions: return {'success': False,
                                     'message': 'Missing --transactions <txid> <txid'}

            sql = sql + ' WHERE t_id IN %(transactions)s'
            sql_args = {'transactions': tuple(transactions)}

        return pylisk.lskutils.db_select(sql, sql_args, self.config_file)

    def blocks(self, rtype, block_id=None, blocks=None):
        """

        """

        sql = ''
        sql_args = None

        if rtype == 'block':

            if not block_id: return {'success': False,
                                     'message': 'Missing --block <block_id>'}

            sql = '''
                SELECT * FROM full_blocks_list WHERE b_id = %(block_id)s'''
            sql_args = {'block_id': block_id}

        elif rtype == 'blocks':

            if not blocks: return {'success': False,
                                   'message': 'Missing --blocks <block_id> <block_id>'}

            sql = '''
                SELECT * FROM full_blocks_list WHERE b_id IN %(block_id)s'''
            sql_args = {'block_id': tuple(blocks)}


        return pylisk.lskutils.db_select(sql, sql_args, self.config_file)

    def delegates(self, rtype, delegate_name=None):
        """

        """

        sql = self.del_addr_sql
        sql_args = None

        if rtype == 'delegates':

            sql = sql + ''' WHERE m."username" IS NOT NULL'''

        elif rtype == 'delegate':

            sql = sql + ''' WHERE m."username" = %(delegate)s'''

            sql_args = {'delegate': delegate_name}

        return pylisk.lskutils.db_select(sql, sql_args, self.config_file)

    def addresses(self, rtype, address=None, addresses=None):
        """

        """

        sql = self.del_addr_sql
        sql_args = None

        if rtype == 'address':

            if not address: return {'success': False,
                                    'message': 'Missing --address <address>'}

            sql = sql + ' WHERE m."address" = %(address)s'
            sql_args = {'address': address}

        elif rtype == 'addresses':

            if not addresses: return {'success': False,
                                      'message': 'Missing --addresses <address> <address>'}

            sql = sql + ' WHERE m."address" IN  %(addresses)s'
            sql_args = {'addresses': tuple(addresses)}

        return pylisk.lskutils.db_select(sql, sql_args, self.config_file)

    def custom(self, rtype, public_key=None, block=None):
        """

        """

        sql = ''
        sql_args = None
        vote_sql = """
            SELECT t."t_id", t."b_height", t."t_blockId",
            t."t_type", t."t_timestamp", t."t_senderId",
            t."t_recipientId", t."t_amount", t."t_fee",
            t."t_signature", t."t_SignSignature", t."t_signatures",
            t."confirmations",
            ENCODE(t."t_senderPublicKey", \'hex\') AS public_key
            FROM votes AS v
            JOIN trs_list AS t ON (t."t_id" = v."transactionId")
            WHERE v."votes" LIKE %(public_key)s;"""

        fork_query = """
            SELECT f."blockTimestamp", f."blockId", f."blockHeight",
            f."previousBlock", f."cause", f."delegatePublicKey"
            FROM forks_stat AS f
            """


        if rtype == 'downvotes' or rtype == 'upvotes':

            if not public_key: return {'success': False,
                                       'message': 'Missing --public-key <pKey>'}

            sql = vote_sql

            if rtype == 'upvotes':
                sql_args = {'public_key': '%+' + public_key + '%'}
            elif rtype == 'downvotes':
                sql_args = {'public_key': '%-' + public_key + '%'}

        elif rtype == 'fork':

            if not block: return {'success': False,
                                  'message': 'Missing --block <blockid>'}

            sql = fork_query + ' WHERE f."blockId" = %(block)s'
            sql_args = {'block': block}

        elif rtype == 'forks':

            sql = fork_query

        return pylisk.lskutils.db_select(sql, sql_args, self.config_file)

