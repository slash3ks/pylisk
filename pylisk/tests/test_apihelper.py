"""
"""

import pytest
from pylisk.helpers import constants
from pylisk.helpers import lisk_api

CONST = constants.init()
LSK = lisk_api.lskAPI(CONST.testnet_url)

def test_get_status():
    """
    """
    resp = LSK.node('status')
    assert resp.get('data')

def test_get_constants():
    """
    """
    resp = LSK.node('constants')
    assert resp.get('data')

def test_get_delegates():
    """
    """
    resp = LSK.delegates('delegates', CONST.offset, CONST.limit)
    assert resp.get('data')

def test_get_forgers():
    """
    """
    resp = LSK.delegates('forgers', CONST.offset, CONST.limit)
    assert resp.get('data')

def test_get_delegate():
    """
    """
    resp = LSK.delegates('delegate', 0, 1, 'slasheks')
    assert resp.get('data')

def test_get_delegate_search():
    """
    """
    resp = LSK.delegates('delegate_search', 0, 2, 'slash')
    assert resp.get('data')

def test_get_forging_statistics():
    """
    """
    resp = LSK.delegates('forging_statistics', CONST.offset, CONST.limit, 'slasheks')
    assert resp.get('data')

def test_get_votes():
    """
    """
    resp = LSK.votes('votes', CONST.offset, CONST.limit, 'slasheks')
    assert resp.get('data')

def test_get_voters():
    """
    """
    resp = LSK.votes('voters', CONST.offset, CONST.limit, 'slasheks')
    assert resp.get('data')

