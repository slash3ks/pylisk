"""

"""
import sys
import json
import requests
import getpass
from pathlib import Path
from subprocess import Popen, PIPE

#TODO read configuration items from config
#TODO catch exceptions as they appear

try:
    import psycopg2
except ImportError:
    sys.exit('Please install psycopg2. sudo pip3 install psycopg2')

def run_cmd(command_string):
    """
    Shells-out and runs commands (lisky)

    Args:
        command_string: A string of the command to run split in a list

    Returns:
        A dictionary of the --json output of lisky or an error dict

    Raises:
        None
    """

    process = Popen(command_string, stdout=PIPE, stderr=PIPE)

    try:
        stdout, stderr = process.communicate()
        return json.loads(stdout.decode('ascii'))
    except:
        #catch and release first then we fry
        raise

def http_get(url, uri, data):
    """
    Requests http get helper function used for all API gets

    Args:
        url (str): The url of the host in question
        uri:(str) The api section of the address
        data: (dict) Any additional data needed in the request

    Returns:
        The Json response of the API call or an error dict

    Raises:
        None
    """

    response = requests.get(url + uri)

    try:
        return response.json()
    except requests.exceptions.ConnectionError:
        #catch and release first then we fry
        raise

def db_select(sql_query, sql_query_args, lisk_config_file):
    """
    Database connection and query helper for PostgreSql

    Args:
        sql_query (str): sql query that will be run
        sql_query_args (dict): additional arguments for the query string

    Returns:
        A dictionary of the data returned by the database query with
        key:value pairs as column_name:column_data

    Raises:
        None
    """

    configuration_items = open_config_file(lisk_config_file)

    # If we get a list, we didnt find the config file so use lisk defaults
    if isinstance(configuration_items, list):
        configuration_items_error = configuration_items[0]
        configuration_items = {
            'db': {
                'database': 'lisk_test',
                'user': getpass.getuser(),
                'host': 'localhost',
                'password': 'password'
            }
        }

    connect_str = "dbname='{database}' user='{user}'"\
                  " host='{host}' password='{password}'"

    conn = psycopg2.connect(connect_str.format(**configuration_items['db']))

    cursor = conn.cursor()
    cursor.execute(sql_query, sql_query_args)

    column_names = [desc[0] for desc in cursor.description]

    results = []
    for row in cursor:
        new_row = [x.tobytes().decode() if isinstance(x, memoryview)
                   else x for x in row]
        results.append(dict(zip(column_names, new_row)))

    return {'data': results}

def check_files(files):
    """

    """

    response = []

    for file_dir in files:

        file_check_pass = Path(file_dir).is_file()

        if not file_check_pass:

            response.append({'success': False,
                             'message': 'Could not find {}'\
                                        ' Please check --install-dir'\
                                        .format(file_dir)})

    return response

def open_config_file(lisk_config_file_dir):
    """

    """

    file_check = check_files([lisk_config_file_dir])
    if file_check and file_check[0]['success'] is False: return file_check

    try:

        with open(lisk_config_file_dir) as lskcfg:

            config = json.load(lskcfg)

            return config

    except:

        raise
