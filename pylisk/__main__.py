#!/usr/bin/env python3
"""
Can use as cmd line lisky, api calls or db calls

They will both be classes and will take a node address
or the directory where lisk is installed
"""

import re
import sys
import argparse
import json
import yaml
from . import lskutils
from . import helpers

def main(args, parser):
    """
        Args:
            args: argparse object with all the needed vars
            parser: needed to access the help
    """

    if args.execution_mode == 'lisky':

        lsky = helpers.lisky.LSKy(args.install_dir)

        if args.opt == 'get' and args.req == 'delegate':

            response = lsky.get(args.req, args.delegate)

        elif args.opt == 'get' and args.req == 'account' or args.req == 'address':

            response = lsky.get(args.req, args.address)

        elif args.opt == 'get' and args.req == 'transaction':

            response = lsky.get(args.req, args.transaction)

        elif args.opt == 'get' and args.req == 'block':

            response = lsky.get(args.req, args.block)

        elif args.opt == 'list' and args.req == 'delegates':

            response = lsky.list(args.req, args.delegates)

        elif args.opt == 'list' and args.req == 'addresses':

            response = lsky.list(args.req, args.addresses)

        elif args.opt == 'list' and args.req == 'transactions':

            response = lsky.list(args.req, args.transactions)

        elif args.opt == 'list' and args.req == 'blocks':

            response = lsky.list(args.req, args.blocks)

        elif args.opt == 'set' and args.req == 'network':

            # Give it the lisky config location + Key + Value to change
            response = lsky.set(args.req, args.lisky_config_path, args.network)

        else:
            print(usage())
            parser.print_help()
            sys.exit(1)

    elif args.execution_mode == 'liskpy':

        lsk_api = helpers.lisk_api.lskAPI(args.url)

        # v1.0.0
        if args.opt == 'get' and args.req == 'constants':

            response = lsk_api.node(args.req)

        elif args.opt == 'get' and args.req == 'status':

            response = lsk_api.node(args.req)

        elif args.opt == 'get' and args.req == 'forging':

            response = lsk_api.node(args.req, args.delegate)

        elif args.opt == 'get' and args.req == 'peers':

            response = lsk_api.peers(args.req, args.offset, args.limit)

        elif args.opt == 'get' and args.req == 'delegates':

            response = lsk_api.delegates(args.req, args.offset, args.limit)

        elif args.opt == 'get' and args.req == 'forgers':

            response = lsk_api.delegates(args.req, args.offset, args.limit)

        elif args.opt == 'get' and args.req == 'delegate' or args.req == 'delegate_search':

            response = lsk_api.delegates(args.req, args.offset, args.limit, args.delegate)

        elif args.opt == 'get' and args.req == 'forging_statistics':

            response = lsk_api.delegates(args.req,
                                         args.offset,
                                         args.limit,
                                         args.delegate,
                                         args.address)

        elif args.opt == 'get' and args.req == 'transaction':

            response = lsk_api.transactions(args.req,
                                            args.offset,
                                            args.limit,
                                            args.transaction)

        elif args.opt == 'get' and args.req == 'transactions':

            response = lsk_api.transactions(args.req,
                                            args.offset,
                                            args.limit,
                                            None)

        elif args.opt == 'get' and args.req == 'votes':

            response = lsk_api.votes(args.req,
                                     args.offset,
                                     args.limit,
                                     args.delegate,
                                     args.address)

        elif args.opt == 'get' and args.req == 'voters':

            response = lsk_api.votes(args.req,
                                     args.offset,
                                     args.limit,
                                     args.delegate,
                                     args.address)

        elif args.opt == 'get' and args.req == 'block':

            response = lsk_api.blocks(args.req,
                                      args.offset,
                                      args.limit,
                                      args.block)

        elif args.opt == 'get' and args.req == 'blocks':

            response = lsk_api.blocks(args.req,
                                      args.offset,
                                      args.limit,
                                      None)


        else:
            print(usage())
            parser.print_help()
            sys.exit(1)

    elif args.execution_mode == 'liskdb':

        lsk_db = helpers.lisk_db.lskDB(args.install_dir + 'config.json')

        if args.opt == 'get' and args.req == 'delegates':

            response = lsk_db.delegates(args.req)

        elif args.opt == 'get' and args.req == 'delegate':

            response = lsk_db.delegates(args.req, args.delegate)

        elif args.opt == 'get' and args.req == 'peers':

            response = lsk_db.peers(args.req)

        elif args.opt == 'get' and args.req == 'block':

            response = lsk_db.blocks(args.req, args.block)

        elif args.opt == 'get' and args.req == 'blocks':

            response = lsk_db.blocks(args.req, None, args.blocks)

        elif args.opt == 'get' and args.req == 'transaction':

            response = lsk_db.transactions(args.req, args.transaction)

        elif args.opt == 'get' and args.req == 'transactions':

            response = lsk_db.transactions(args.req, None, args.transactions)

        elif args.opt == 'get' and args.req == 'address':

            response = lsk_db.addresses(args.req, args.address)

        elif args.opt == 'get' and args.req == 'addresses':

            response = lsk_db.addresses(args.req, None, args.addresses)

        elif args.opt == 'get' and args.req == 'upvotes':

            response = lsk_db.custom(args.req, args.public_key)

        elif args.opt == 'get' and args.req == 'downvotes':

            response = lsk_db.custom(args.req, args.public_key)

        elif args.opt == 'get' and args.req == 'fork':

            response = lsk_db.custom(args.req, None, args.block)

        elif args.opt == 'get' and args.req == 'forks':

            response = lsk_db.custom(args.req)

        else:
            print(usage())
            parser.print_help()
            sys.exit(1)

    else:
        print(usage())
        parser.print_help()
        sys.exit(1)

    if args.yaml and args.execution_mode == 'liskpy':
        print(yaml.dump(response))
    else:
        print(json.dumps(response, indent=3))


def usage():
    """
    Usage examples for pylisk

    Args:
        None

    Returns:
        A string with examples on how to use pylisk

    Raises:
        None
    """

    return """Examples:
    v1.0.0
    python3 -m pylisk get status
    python3 -m pylisk get constants

    python3 -m pylisk get peers
    python3 -m pylisk get peers --limit 1
    python3 -m pylisk get peers --offset 2
    python3 -m pylisk get peers --offset 2 --limit 2

    python3 -m pylisk get delegates
    python3 -m pylisk get delegates --limit 2
    python3 -m pylisk get delegates --offset 2
    python3 -m pylisk get delegates --offset 2 --limit 2

    python3 -m pylisk get forging --delegate slasheks

    python3 -m pylisk get forgers

    python3 -m pylisk get delegate --delegate slasheks
    python3 -m pylisk get delegate_search --delegate slash

    python3 -m pylisk get forging_statistics --delegate genesis_80

    python3 -m pylisk get transactions
    python3 -m pylisk get transactions --limit 1
    python3 -m pylisk get transactions --offset 1
    python3 -m pylisk get transactions --offset 1 --limit 1
    python3 -m pylisk get transaction --transaction 9046966103496425914

    python3 -m pylisk get blocks
    python3 -m pylisk get blocks --limit 1
    python3 -m pylisk get blocks --offset 1
    python3 -m pylisk get blocks --offset 1 --limit 1
    python3 -m pylisk get block --block 9046966103496425914

    python3 -m pylisk get votes --delegate slasheks
    python3 -m pylisk get voters --delegate slasheks
    """


if __name__ == '__main__':

    PARSER = argparse.ArgumentParser()

    PARSER.add_argument(dest='opt', action='store',
                        choices=['get', 'list', 'set'],
                        help='What action to take (get)')

    PARSER.add_argument(dest='req', action='store',
                        choices=['address', 'account', 'delegate', 'delegates',
                                 'peers', 'votes', 'voters', 'status', 'sync',
                                 'transaction', 'transactions', 'addresses',
                                 'block', 'blocks', 'network', 'upvotes',
                                 'downvotes', 'forging', 'fork', 'forks', 
                                 'constants', 'forgers', 'delegate_search',
                                 'forging_statistics'],
                        help='')

    PARSER.add_argument('-e', '--exec', dest='execution_mode', action='store',
                        default='liskpy', help='')

    #LISK Commander (bleh)
    PARSER.add_argument('-i','--install-dir', dest='install_dir', action='store',
                        default='/home/lisk/lisk-Linux-x86_64/',
                        help='Lisk install directory')

    PARSER.add_argument('-li','--lisky-config', dest='lisky_config_path',
                        action='store',
                        default='/home/lisk/.lisky/config.json',
                        help='Lisk install directory')

    PARSER.add_argument('--delegates', dest='delegates', action='store',
                        nargs='+', help='Delegate names')

    PARSER.add_argument('--blocks', dest='blocks', action='store',
                        nargs='+', help='Block IDs')

    PARSER.add_argument('--addresses', dest='addresses', action='store',
                        nargs='+', help='Delegate addresses')

    PARSER.add_argument('--transactions', dest='transactions', action='store',
                        nargs='+', help='Transactions by id')

    #LSK_API side of the house
    PARSER.add_argument('-u','--url', dest='url', action='store',
                        default='http://testnet.lisk.io',
                        help='Lisk enpoint url')

    PARSER.add_argument('-o','--offset', dest='offset', action='store',
                        type=int, help='offset API query option')

    PARSER.add_argument('-l','--limit', dest='limit', action='store',
                        type=int, help='limit API query option')

    #LISKDB side of the house

    #All sides, no one is angry here
    PARSER.add_argument('-n','--network', dest='network', action='store',
                        choices=['mainnet', 'testnet'], default='testnet',
                        help='Network name')

    PARSER.add_argument('-d','--delegate', dest='delegate', action='store',
                        help='Delegate name')

    PARSER.add_argument('-b','--block', dest='block', action='store',
                        help='Delegate name')

    PARSER.add_argument('-t','--transaction', dest='transaction', action='store',
                        help='Delegate name')

    PARSER.add_argument('-a', '--address', dest='address', action='store',
                        help='Delegate address')

    PARSER.add_argument('-p', '--public-key', dest='public_key', action='store',
                        help='')

    PARSER.add_argument('--yaml', dest='yaml', action='store_true',
                        help='Delegate address')

    if len(sys.argv[1:]) < 2: print(usage())

    ARGS = PARSER.parse_args()

    main(ARGS, PARSER)
