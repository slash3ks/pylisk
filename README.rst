# pylisk

Python lisk API library package 


```
git clone https://github.com/slasheks/pylisk.git
cd pylisk
python3 pylisk
```

Examples:

    python3 pylisk lisky get delegate --delegate slasheks
    python3 pylisk lisky get transaction --transaction 15546460506328965550
    python3 pylisk lisky get block --block 5140353407898744576
    python3 pylisk lisky get address --address 17589464102226817833L
    python3 pylisk lisky set network --network testnet
    python3 pylisk lisky set network --network mainnet

    python3 pylisk lisky list delegates --delegates slasheks gr33ndrag0n
    python3 pylisk lisky list transactions --transactions 15546460506328965550
    python3 pylisk lisky list blocks --blocks 5140353407898744576 15887592492782308955
    python3 pylisk lisky list addresses --addresses 17589464102226817833L 194109334904015388L

    python3 pylisk liskpy get delegate --delegate slasheks
    python3 pylisk liskpy get transaction --transaction 7881560633276667078
    python3 pylisk liskpy get next_forgers
    python3 pylisk liskpy get delegates
    python3 pylisk liskpy get version
    python3 pylisk liskpy get status
    python3 pylisk liskpy get sync

    python3 pylisk liskdb get delegate --delegate slasheks
    python3 pylisk liskdb get delegates
    python3 pylisk liskdb get peers
    python3 pylisk liskdb get block --block 10857411233141233678
    python3 pylisk liskdb get blocks --blocks 10857411233141233678 10857411233141233678
    python3 pylisk liskdb get transaction --transaction 7881560633276667078
    python3 pylisk liskdb get transactions --transactions 7881560633276667078 2484625766645174446
    python3 pylisk liskdb get upvotes --public-key 7beb5f1e8592022fe5272b45eeeda6a1b6923a801af6e1790933cc6a78ed95a1
    python3 pylisk liskdb get downvotes --public-key 7beb5f1e8592022fe5272b45eeeda6a1b6923a801af6e1790933cc6a78ed95a1
    python3 pylisk liskdb get fork --block 13890548296204040564
    python3 pylisk liskdb get forks
    
Usage:

    pylisk [-h] [-i INSTALL_DIR] [--delegates DELEGATES [DELEGATES ...]]
              [--blocks BLOCKS [BLOCKS ...]]
              [--addresses ADDRESSES [ADDRESSES ...]]
              [--transactions TRANSACTIONS [TRANSACTIONS ...]] [-u URL]
              [-d DELEGATE] [-b BLOCK] [-t TRANSACTION] [-a ADDRESS]
              {lisky,liskpy,liskdb} {get,list}
              {address,account,delegate,delegates,version,peers,votes,status,sync,transaction,addresses,block,blocks}
