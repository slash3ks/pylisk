from setuptools import setup

setup(name='pylisk',
      version='0.1',
      description='Library to interact with the Lisk blockchain',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Internet :: Blockchain',
      ],
      url='https://github.com/slasheks/pylisk',
      author='slasheks',
      author_email='slasheks@protonmail.com',
      license='MIT',
      packages=['pylisk'],
      install_requires=[
          'pyYaml',
          'requests',
          'psycopg2'
      ],
      zip_safe=False)
